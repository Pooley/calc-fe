import React from "react";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";
import Button from "./Button";

/**
 * Take memory value and treat as user input.
 */
const MemoryRecallButton: React.FC<NumberPadPositionalInterface & CalculationStateInterface> = ({state, setState, ...props}:(NumberPadPositionalInterface & CalculationStateInterface)) => {
    const value = "MR";
    
    const handleButtonClick = () => {
        let newState = {
            ...state
        }

        if (/\d+$/.test(state.calculationString)) {
            // replace last stack
            let popped = `${newState.calculationStack.pop()}`.length;
            newState.calculationString = newState.calculationStack.join('');
        }
        
        newState.calculationString += newState.memory;
        newState.calculationValue = newState.memory;
        newState.calculationStack.push(newState.memory);
        
        setState(newState);
    }

    return (
        <>
            <Button value={value} {...props} handleButtonClick={handleButtonClick}/>
        </>
    );
};

export default MemoryRecallButton;