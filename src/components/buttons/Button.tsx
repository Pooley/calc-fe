import React from "react";
import { ButtonInterface } from "../../interfaces/ButtonInterface";
import styled from "styled-components";

interface HandlingButton extends ButtonInterface {
    handleButtonClick: () => void;
}

const Button: React.FC<HandlingButton> = ({value, colPos, rowPos,handleButtonClick}:HandlingButton) => {

    /**
     * Aware this causes console warning message per instance due to dynamic element created in 
     * nested components. It is more to day 'here is a styled' than anything else as imho inline
     * defeats purpose of css files even though there is more 'self-containedism' by doing so.
     */
    const GridPos = styled.section`
        grid-column: col ${colPos};
        grid-row: row ${rowPos};
    `;

    const handleOnClick = (event:any) => {
        handleButtonClick();
    }

    return (
        <>
            <GridPos>
                <button onClick={handleOnClick}>{value}</button>
            </GridPos>
        </>
    );
};

export default Button;