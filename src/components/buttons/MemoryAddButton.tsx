import React from "react";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";
import Button from "./Button";

/**
 * Add currentValue (result) to value in memory (which is 0 by default)
 */
const MemoryAddButton: React.FC<NumberPadPositionalInterface & CalculationStateInterface> = ({state, setState, ...props}:(NumberPadPositionalInterface & CalculationStateInterface)) => {
    const value = "M+";

    const handleButtonClick = () => {
        let newState = {
            ...state
        }

        newState.memory = state.memory + state.calculationValue;
        
        setState(newState);
    }

    return (
        <>
            <Button value={value} {...props} handleButtonClick={handleButtonClick}/>
        </>
    );
};

export default MemoryAddButton;