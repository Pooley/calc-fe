import React from "react";
import OperationButton from "./OperationButton";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";

const DivisionButton: React.FC<NumberPadPositionalInterface & CalculationStateInterface> = (props:(NumberPadPositionalInterface & CalculationStateInterface)) => {
    const value = "/";

    return (
        <>
            <OperationButton value={value} {...props}/>
        </>
    );
};

export default DivisionButton;