import React from "react";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";
import OperationButton from "./OperationButton";

const SubtractionButton: React.FC<NumberPadPositionalInterface & CalculationStateInterface> = (props:(NumberPadPositionalInterface & CalculationStateInterface)) => {
    const value = "-";

    return (
        <>
            <OperationButton value={value} {...props}/>
        </>
    );
};

export default SubtractionButton;