import React from "react";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import Button from "./Button";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";

const HistoryButton: React.FC<NumberPadPositionalInterface & CalculationStateInterface> = ({ state, setState,...props}:(NumberPadPositionalInterface & CalculationStateInterface)) => {
    const value= "H";

    const handleButtonClick = () => {
        alert(state.history.join('\n'));
    }

    return (
        <>
            <Button value={value} {...props} handleButtonClick={handleButtonClick}/>
        </>
    );
};

export default HistoryButton;