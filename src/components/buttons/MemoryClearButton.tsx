import React from "react";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";
import Button from "./Button";

/**
 * Clear the memory store.
 */
const MemoryAddButton: React.FC<NumberPadPositionalInterface & CalculationStateInterface> = ({state, setState, ...props}:(NumberPadPositionalInterface & CalculationStateInterface)) => {
    const value = "MC";

    const handleButtonClick = () => {
        let newState = {
            ...state
        }

        newState.memory = 0;
        
        setState(newState);
    }

    return (
        <>
            <Button value={value} {...props} handleButtonClick={handleButtonClick}/>
        </>
    );
};

export default MemoryAddButton;