import React from "react";
import DigitButton from "./DigitButton";
import DecimalButton from "./DecimalButton";
import AdditionButton from "./AdditionButton";
import DivisionButton from "./DivisionButton";
import MultiplicationButton from "./MultiplicationButton";
import SubtractionButton from "./SubtractionButton";
import CalculationButton from "./CalculationButton";
import { CalculationStateType } from "../model/CalculationStateType";
import MemoryAddButton from "./MemoryAddButton";
import MemoryClearButton from "./MemoryClearButton";
import MemoryRecallButton from "./MemoryRecallButton";
import MemoryRemoveButton from "./MemoryRemoveButton";
import ExponentialButton from "./ExponentialButton";
import HistoryButton from "./HistoryButton";
import PercentageButton from "./PercentageButton";
import SquareRootButton from "./SquareRootButton";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";

class ButtonFactory extends React.Component {
    static create = (value:any, rowIndex:number, columnIndex:number, state: CalculationStateType, setState:(state:CalculationStateType)=>void) => {
        
        let props:(NumberPadPositionalInterface & CalculationStateInterface) = {
            rowPos: rowIndex,
            colPos: columnIndex,
            state: state,
            setState: setState,
        }
        
        if (isNaN(value)) {
            switch (value) {
                case ".": return <DecimalButton key={value} {...props}/>;
                case "+": return <AdditionButton key={value} {...props}/>;
                case "-": return <SubtractionButton key={value} {...props}/>;
                case "*": return <MultiplicationButton key={value} {...props}/>;
                case "/": return <DivisionButton key={value} {...props}/>;
                case "=": return <CalculationButton key={value} {...props}/>;
                case "M+": return <MemoryAddButton key={value} {...props}/>;
                case "M-": return <MemoryRemoveButton key={value} {...props}/>;
                case "MC": return <MemoryClearButton key={value} {...props}/>;
                case "MR": return <MemoryRecallButton key={value} {...props}/>;
                case "%": return <PercentageButton key={value} {...props}/>;
                case "√": return <SquareRootButton key={value} {...props}/>;
                case "^": return <ExponentialButton key={value} {...props}/>;
                case "Hst": return <HistoryButton key={value} {...props}/>;
                default: return <></>;
            }
        }

        return <DigitButton key={value} value={value} {...props}/>;
    }
  }

export default ButtonFactory;