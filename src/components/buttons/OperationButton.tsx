import React from "react";
import Button from "./Button";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";
import { ButtonInterface } from "../../interfaces/ButtonInterface";

const OperationButton: React.FC<ButtonInterface & CalculationStateInterface> = ({value, state, setState, ...props}:(ButtonInterface & CalculationStateInterface)) => {

    const handleButtonClick = () => {
        let newState = {
            ...state
        }
        
        // last char is =, continue with calculatedValue
        if (/=$/.test(state.calculationString)) {
            newState.calculationString = `${state.calculationValue}`;
            newState.calculationStack = [state.calculationValue];
        }

        newState.calculationStack.push(value);
        newState.calculationString += value;
        // TODO: instead of placing the operand value (+_*^/) adding a concrete Operand
        // and refactoring the calculationbutton logic may be better and remove use of
        // primitives.

        setState(newState);
    }

    return (
        <>
            <Button value={value} {...props} handleButtonClick={handleButtonClick}/>
        </>
    );
};

export default OperationButton;