import React from "react";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import OperationButton from "./OperationButton";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";

const ExponentialButton: React.FC<NumberPadPositionalInterface & CalculationStateInterface> = (props:(NumberPadPositionalInterface & CalculationStateInterface)) => {
    const value= "^";
    return (
        <>
            <OperationButton value={value} {...props}/>
        </>
    );
};

export default ExponentialButton;