import React from "react";
import { ButtonInterface } from "../../interfaces/ButtonInterface";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";
import Button from "./Button";

interface DigitButtonInterface extends ButtonInterface {
    value : number;
}

const DigitButton: React.FC<DigitButtonInterface & CalculationStateInterface> = ({value, state, setState, ...props}:(DigitButtonInterface & CalculationStateInterface)) => {

    const handleButtonClick = () => {
        let newState = {
            ...state
        }

        let isDecimal = /\.$/gm.test(state.calculationString); // get around input typed to number not dispaying '.' unless there is a decimal value.
        let isOperand = /[+|\-|*|/|^]$/.test(state.calculationString); // last char is +-*/
        let isSubmitted = /=$/.test(state.calculationString); // last char is =, clean out stack

        if (isSubmitted) {
            newState.calculationValue = 0;
            newState.calculationString = '';
        }

        newState.calculationString += value;

        if (newState.calculationString.trim().length === 1 || isOperand) {
            newState.calculationValue = value;
            newState.calculationStack.push(newState.calculationValue);
        } else {
            newState.calculationValue = parseFloat(`${newState.calculationValue}${(isDecimal)?'.':''}${value}`);
            newState.calculationStack[newState.calculationStack.length-1] = newState.calculationValue;
        }

        setState(newState);
    }

    return (
        <>
            <Button value={value} {...props} handleButtonClick={handleButtonClick}/>
        </>
    );
};

export default DigitButton;