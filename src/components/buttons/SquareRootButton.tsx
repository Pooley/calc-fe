import React from "react";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import Button from "./Button";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";

const SquareRootButton: React.FC<NumberPadPositionalInterface & CalculationStateInterface> = ({state, setState, ...props}:(NumberPadPositionalInterface & CalculationStateInterface)) => {
    const value= "√";

    const handleButtonClick = () => {
        let newState = {
            ...state
        }

        newState.calculationStack.pop();
        newState.calculationValue = Math.sqrt(state.calculationValue);
        newState.calculationString = `${newState.calculationStack.join('')}√(${newState.calculationValue})`;
        newState.calculationStack.push(newState.calculationValue);

        setState(newState);
    }

    return (
        <>
            <Button value={value} {...props} handleButtonClick={handleButtonClick}/>
        </>
    );
};

export default SquareRootButton;