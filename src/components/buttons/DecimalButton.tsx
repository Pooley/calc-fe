import React from "react";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import Button from "./Button";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";

const DecimalButton: React.FC<NumberPadPositionalInterface & CalculationStateInterface> = ({state,setState, ...props}:(NumberPadPositionalInterface & CalculationStateInterface)) => {
    const value = ".";

    const handleButtonClick = () => {
        let newState = {
            ...state
        }

        newState.calculationString += value;
        newState.calculationValue = Number.parseFloat(`${newState.calculationValue}${value}0`)
        // note that value:number typed input field does not show decimal point unless a decimal value has been set, e.g. 1.3.
        
        setState(newState);
    }

    return (
        <>
            <Button value={value} {...props} handleButtonClick={handleButtonClick}/>
        </>
    );
};

export default DecimalButton;