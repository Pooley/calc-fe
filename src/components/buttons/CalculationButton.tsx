import React from "react";
import Button from "./Button";
import { NumberPadPositionalInterface } from "../../interfaces/NumberPadPositionalInterface";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";
import MultiplicationOperand from "../operands/MultiplicationOperand";
import AdditionOperand from "../operands/AdditionOperand";
import DivisionOperand from "../operands/DivisionOperand";
import SubtractionOperand from "../operands/SubtractionOperand";
import ExponentialOperand from "../operands/ExponentialOperand";

const CalculationButton: React.FC<NumberPadPositionalInterface & CalculationStateInterface> = ({colPos, rowPos, state, setState}:(NumberPadPositionalInterface & CalculationStateInterface)) => {
   
    const visitors = [
        new SubtractionOperand(),
        new AdditionOperand(),
        new DivisionOperand(),
        new MultiplicationOperand(),
        new ExponentialOperand(),
    ]

    /**
     * Recursively iterate through the operands in PEMBAS sequence replacing executed operands and its
     * pair of input with the operation value for all occurances of the current operand. The final return
     * is a single value in the stack.
     * 
     * @param stack tokenised user input expression
     * @param operands sequence of operations to perform
     * @param operandIndex recursive loop index.
     * @returns potentially modified stack
     */
    const calculatePair:any = (stack:Array<string|number>,operands:Array<string>,operandIndex:number) => {
        if (operandIndex === operands.length) {
            return stack;
        }

        for (let i=0; i < stack.length; i++) {
            let item = stack[i];
            if (isNaN(Number(item))) {

                // visitable
                let prev = Number(stack[i-1]);
                let next = Number(stack[i+1]);


                if (item === operands[operandIndex]) {
                    let results = visitors.map(v => {
                        return v.visit(prev,next,`${item}`)
                    });
                    let opValue = results.find(x => x !== undefined);

        /*         // procedural
                 if (item === operands[operandIndex]) {
                     let prev = Number(stack[i-1]);
                     let next = Number(stack[i+1]);
                     let opValue = 0;
                    
                     switch (operands[operandIndex]) {
                         case "+": opValue = prev + next; break;
                         case "-": opValue = prev - next; break;
                         case "*": opValue = prev * next; break;
                         case "/": opValue = prev / next; break;
                 }
        */
                    if (opValue !== undefined) {
                        stack.splice(i-1,3); // remove the executed digits and operand and replace it with the operation's value.
                        stack = [
                            ...stack.slice(0, i-1),
                            opValue,
                            ...stack.slice(i-1)
                        ]
                    }
                     i=0; // reset to loop from beginning
        //         }
                }
             }
         }
        return calculatePair(stack, operands, operandIndex+1);
    }

    const handleButtonClick = () => {
        let newState = {
            ...state
        }
        
        let stack = calculatePair([...newState.calculationStack],["^","*","/","+","-"],0);

        newState.calculationString += "=";
        newState.calculationValue = Number(stack[0]);
        newState.history.push(`${newState.calculationString} ${newState.calculationValue}`);
        newState.calculationStack = [newState.calculationValue];
       
        setState(newState);
    }

    return (
        <>
            <Button value='=' colPos={colPos} rowPos={rowPos} handleButtonClick={handleButtonClick}/>
        </>
    );
};

export default CalculationButton;