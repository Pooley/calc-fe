type CalculationStateType = {
    calculationValue: number;
    calculationString: string;
    calculationStack: Array<string|number>;
    memory: number;
    history: Array<string>;
}

export type {CalculationStateType}