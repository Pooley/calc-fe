import React from "react";

class AdditionOperand extends React.Component {

    constructor() {
        super({});
    }

    visit = (lhs:number, rhs:number,operand:string) => {
        if ("+" === operand) {
            return lhs + rhs;
        }
    }
}

export default AdditionOperand;