import React, { useState } from "react";

const SignInUp: React.FC = () => {

    const [siUsername, setSiUsername] = useState('');
    const [siPassword, setSiPassword] = useState('');
    const [failedSignIn, setFailedSignIn] = useState(false);
    const [failedSignUp, setFailedSignUp] = useState(false);
    const [successfulSignUp,setSuccessfulSignUp] = useState(false);
    const [siConfirmPassword, setSiConfirmPassword] = useState('');

    const handleSignIn = (event:any) => {
        if (siUsername?.trim() && siPassword?.trim()) {
            setFailedSignIn(false);
            document.dispatchEvent(new CustomEvent('signIn',{detail:true}));
            setSiUsername('');
            setSiPassword('');
        } else {
            setFailedSignIn(true);
            document.dispatchEvent(new CustomEvent('signIn',{detail:false}));
        }
    }

    const handleSignUp = (event:any) => {
        if (siPassword !== siConfirmPassword) {
            setFailedSignUp(true);
            setSuccessfulSignUp(false);
        } else if (siUsername?.trim() && siPassword?.trim() && siConfirmPassword?.trim()) {
            setFailedSignUp(false);
            setSuccessfulSignUp(true);

            setSiUsername('');
            setSiPassword('');
            setSiConfirmPassword('');
        } else {
            setFailedSignUp(true);
            setSuccessfulSignUp(false);
        }
    }

    return (
        <>
            <p>Note that both identity forms share state for brevity of this example.</p>
            <div className="signInForm">
                <h1>Sign In</h1>
                {failedSignIn && <p className="warning">Enter value credentials.</p>}
                <div>
                    <label>Username:</label>
                    <input id="si-username" value={siUsername} onChange={(e)=> setSiUsername(e.target.value)} />
                </div>
                <div>
                    <label>Password:</label>
                    <input id="si-password" type="password" value={siPassword} onChange={(e)=> setSiPassword(e.target.value)} />
                </div>
                <button onClick={handleSignIn}>Sign In</button>
            </div>
            <p>or</p>
            <div className="signInForm">
                <h1>Sign Up</h1>
                {failedSignUp && <p className="warning">Enter value credentials or passwords mismatch.</p>}
                {successfulSignUp && <p>Sign In after Signing Up.</p>}
                <div>
                    <label>Username:</label>
                    <input id="su-username" value={siUsername} onChange={(e)=> setSiUsername(e.target.value)} />
                </div>
                <div>
                    <label>Password:</label>
                    <input id="su-password" type="password" value={siPassword} onChange={(e)=> setSiPassword(e.target.value)} />
                </div>
                <div>
                    <label>Password:</label>
                    <input id="su-confirmpassword" type="password" value={siConfirmPassword} onChange={(e)=> setSiConfirmPassword(e.target.value)} />
                </div>
                <button onClick={handleSignUp}>Sign Up</button>
            </div>
        </>
    );
};

export default SignInUp;