import React from "react";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";

const CalculationBar: React.FC<CalculationStateInterface> = ({state}:CalculationStateInterface) => {

    return (
        <>  
            <div className="calculationBar">
                <div className="calculationString">
                    {state.calculationString}
                </div>
                <div>
                    <input readOnly={true} value={state.calculationValue}/>
                </div>
            </div>
        </>
    );
};

export default CalculationBar;