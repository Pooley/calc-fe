import React from "react";
import ButtonFactory from "../buttons/ButtonFactory";
import { CalculationStateInterface } from "../../interfaces/CalculationStateInterface";

const NumberPad: React.FC<CalculationStateInterface> = ({state, setState}:CalculationStateInterface) => {

    const matrix = [
            ['M+','M-','MR','MC'],
            ['%','√','Hst','^'],
            [7,8,9,'+'],
            [4,5,6,'-'],
            [1,2,3,'*'],
            ["=",0,".",'/']
        ]
    ;

    const renderMatrix = () => {
        return matrix.map((r,rindex) => {
            return r.map((c,cindex) => {
                return ButtonFactory.create(c,rindex,cindex,state,setState);
            })
        })
    }

    return (
        <>  
            <div className="numberPad">
                {renderMatrix()}
            </div>
        </>
    );
};

export default NumberPad;