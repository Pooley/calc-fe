import React, { useState } from "react";

const TopBar: React.FC = () => {
    const [userStatus, setUserStatus] = useState(false);

    /**
     * Receive event dispatched from SignInUp.
     */
    document.addEventListener('signIn', ((event: CustomEvent) => {
        setUserStatus(event.detail);
    }) as EventListener);

    return (
        <>
            <div className="topBar">
                <div className="userStatus">{`Logged In: ${userStatus}`}</div>
            </div>
        </>
    )
}

export default TopBar;