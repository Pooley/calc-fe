import React, { useState } from 'react';
import './App.css';
import NumberPad from './components/calculator/NumberPad';
import CalculationBar from './components/calculator/CalculationBar';
import {CalculationStateType} from './components/model/CalculationStateType';
import SignInUp from './components/identity/SignInUp';
import TopBar from './components/TopBar';

function App() {
  let state : CalculationStateType = {
    calculationValue: 0,
    calculationString: '',
    calculationStack: [],
    memory: 0,
    history:[],
  }

  const [calculatorState, setCalculatorState] = useState(state);

  return (
    <div className="App">
      <TopBar />
      <CalculationBar state={calculatorState} setState={setCalculatorState}/>
      <NumberPad state={calculatorState} setState={setCalculatorState}/>
      <hr/>
      <SignInUp/>
    </div>
  );
}

export default App;
