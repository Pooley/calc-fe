import { CalculationStateInterface } from "./CalculationStateInterface";

export interface NumberPadPositionalInterface {
    rowPos: number;
    colPos: number;
}