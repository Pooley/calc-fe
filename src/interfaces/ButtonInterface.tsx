import { CalculationStateType } from "../components/model/CalculationStateType";
import { NumberPadPositionalInterface } from "./NumberPadPositionalInterface";

export interface ButtonInterface extends NumberPadPositionalInterface {
    value: any;
}