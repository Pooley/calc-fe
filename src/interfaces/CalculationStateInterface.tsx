import { CalculationStateType } from "../components/model/CalculationStateType";

export interface CalculationStateInterface {
    state: CalculationStateType;
    setState: (state:CalculationStateType) => void;
}